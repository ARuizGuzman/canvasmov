alert("Bienvenidos");

function inicializar() {
    let lienzo= document.getElementById('miLienzo');
    lienzo.width = window.innerWidth;
    lienzo.height = window.innerHeight;
    ctx = lienzo.getContext('2d');
    ctx.stroke();
    ctx.translate(0, window.innerHeight/2);
    ctx.scale(1, -1);
 
}

function circulitos(x, y, radio) {
    ctx.beginPath();
    ctx.arc(x, y, radio, 0, 2 * Math.PI, true);
    ctx.fill();
}

function color() {
    ctx.fillStyle = "green";  
}

function rectangulo(){

    ctx.strokeRect(0, 0, 10, 20);
}

function simular(){
    color();
    rectangulo();
    let t=0, y=0, x=0;
    let vo=document.getElementById('Vo').value;
    let wo=document.getElementById('Wo').value;
    wo=(wo*Math.PI)/180;
    while(y>=0){      
       x=vo*Math.cos(wo)*t;
       y=vo*Math.sin(wo)*t-(0.5)*9.8* Math.pow(t,2);
       t+=0.09;
       circulitos(x, y,2);
       rectangulo(x,y);

    }

}